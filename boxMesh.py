""" Creates the same files that would be created by OpenFOAM's blockMesh if the
 * mesh is rectangular cuboidal domain divided into smaller identical
 * rectangular cuboids.
 * Using this code, I was able to generate a 500x500x500 mesh that I couldn't
 * generate using blockMesh on my computer with 64 GB RAM. It takes 70% more
 * time than the OpenFOAM's blockMesh, but requires 40% less memory.
 *
 * See comments on void boxMesh(int Nx, int Ny, int Nz, double Lx, double Ly, double Lz)
 * below for an example of the equivalent blockMeshDict dictionary
 *
 * This code requires that the directory constant/polyMesh exits.
 *
 * Author: Thomas Oliveira
 * Contact: Gmail - thomas.oliveira """

import os
import sys

def writeBoundary(nFaces, nExternalFaces):

    currentFileName = "constant/polyMesh/boundary"
    if not os.path.exists(os.path.dirname(currentFileName)):
        os.makedirs(os.path.dirname(currentFileName))
    currentFile = open(currentFileName, "w")

    currentFile.write("/*--------------------------------*- C++ -*----------------------------------*\\\n")
    currentFile.write("| =========                 |                                                 |\n")
    currentFile.write("| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |\n")
    currentFile.write("|  \\\\    /   O peration     | Version:  2.3.0                                 |\n")
    currentFile.write("|   \\\\  /    A nd           | Web:      www.OpenFOAM.org                      |\n")
    currentFile.write("|    \\\\/     M anipulation  |                                                 |\n")
    currentFile.write("\*---------------------------------------------------------------------------*/\n")
    currentFile.write("FoamFile\n")
    currentFile.write("{\n")
    currentFile.write("    version     2.0;\n")
    currentFile.write("    format      ascii;\n")
    currentFile.write("    class       polyBoundaryMesh;\n")
    currentFile.write("    location    \"constant/polyMesh\";\n")
    currentFile.write("    object      boundary;\n")
    currentFile.write("}\n")
    currentFile.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n")
    currentFile.write("\n")
    currentFile.write("1\n")
    currentFile.write("(\n")
    currentFile.write("    defaultFaces\n")
    currentFile.write("    {\n")
    currentFile.write("        type            empty;\n")
    currentFile.write("        inGroups        1(empty);\n")

    currentFile.write("        nFaces          " + str(nExternalFaces) + ";\n")
    currentFile.write("        startFace       " + str(nFaces-nExternalFaces) + ";\n")

    currentFile.write("    }\n")
    currentFile.write(")\n")
    currentFile.write("\n")
    currentFile.write("// ************************************************************************* //\n")
    currentFile.close()

    return 0

def writeFaces(nCells, nFaces, nExternalFaces, Nx, Ny, Nz):

    currentFileName = "constant/polyMesh/faces"
    if not os.path.exists(os.path.dirname(currentFileName)):
        os.makedirs(os.path.dirname(currentFileName))
    currentFile = open(currentFileName, "w")

    currentFile.write("/*--------------------------------*- C++ -*----------------------------------*\\\n")
    currentFile.write("| =========                 |                                                 |\n")
    currentFile.write("| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |\n")
    currentFile.write("|  \\\\    /   O peration     | Version:  2.3.0                                 |\n")
    currentFile.write("|   \\\\  /    A nd           | Web:      www.OpenFOAM.org                      |\n")
    currentFile.write("|    \\\\/     M anipulation  |                                                 |\n")
    currentFile.write("\*---------------------------------------------------------------------------*/\n")
    currentFile.write("FoamFile\n")
    currentFile.write("{\n")
    currentFile.write("    version     2.0;\n")
    currentFile.write("    format      ascii;\n")
    currentFile.write("    class       faceList;\n")
    currentFile.write("    location    \"constant/polyMesh\";\n")
    currentFile.write("    object      faces;\n")
    currentFile.write("}\n")
    currentFile.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n")
    currentFile.write("\n")
    currentFile.write("\n")

    currentFile.write(str(nFaces)+"\n(\n")

    for i in range(nCells):
        faces = pointsOfInternalFacesOfCell(i, Nx, Ny, Nz)
        for points in faces:
            currentFile.write("4(" + str(points[0]) + " " + str(points[1]) + " " + str(points[2]) + " "+ str(points[3]) + ")\n")

    for i in range(nExternalFaces):
        points = pointsOfExternalFace(i, Nx, Ny, Nz);
        currentFile.write("4(" + str(points[0]) + " " + str(points[1]) + " " + str(points[2]) + " "+ str(points[3]) + ")\n")

    currentFile.write(")\n")
    currentFile.write("\n")
    currentFile.write("\n")
    currentFile.write("// ************************************************************************* //\n")
    currentFile.close()

    return 0

def writeNeighbour(nPoints, nCells, nFaces, nExternalFaces, Nx, Ny, Nz):

    currentFileName = "constant/polyMesh/neighbour"
    if not os.path.exists(os.path.dirname(currentFileName)):
        os.makedirs(os.path.dirname(currentFileName))
    currentFile = open(currentFileName, "w")

    currentFile.write("/*--------------------------------*- C++ -*----------------------------------*\\\n")
    currentFile.write("| =========                 |                                                 |\n")
    currentFile.write("| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |\n")
    currentFile.write("|  \\\\    /   O peration     | Version:  2.3.0                                 |\n")
    currentFile.write("|   \\\\  /    A nd           | Web:      www.OpenFOAM.org                      |\n")
    currentFile.write("|    \\\\/     M anipulation  |                                                 |\n")
    currentFile.write("\*---------------------------------------------------------------------------*/\n")
    currentFile.write("FoamFile\n")
    currentFile.write("{\n")
    currentFile.write("    version     2.0;\n")
    currentFile.write("    format      ascii;\n")
    currentFile.write("    class       labelList;\n")
    currentFile.write("    note        \"nPoints: " + str(nPoints) + " nCells: " + str(nCells) + " nFaces: "+ str(nFaces) + " nInternalFaces: " + str(nFaces-nExternalFaces) + "\";\n")
    currentFile.write("    location    \"constant/polyMesh\";\n")
    currentFile.write("    object      neighbour;\n")
    currentFile.write("}\n")
    currentFile.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n")
    currentFile.write("\n")
    currentFile.write("\n")

    currentFile.write(str(nFaces-nExternalFaces )+"\n(\n")

    for i in range(nCells):
        neighbours = neighboursOfInternalFacesOfCell(i, Nx, Ny, Nz)
        for neighbour in neighbours:
            currentFile.write(str(neighbour)+"\n")

    currentFile.write(")\n")
    currentFile.write("\n")
    currentFile.write("\n")
    currentFile.write("// ************************************************************************* //\n")
    currentFile.close()

    return 0

def writeOwner(nPoints, nCells, nFaces, nExternalFaces, Nx, Ny, Nz):

    currentFileName = "constant/polyMesh/owner"
    if not os.path.exists(os.path.dirname(currentFileName)):
        os.makedirs(os.path.dirname(currentFileName))
    currentFile = open(currentFileName, "w")

    currentFile.write("/*--------------------------------*- C++ -*----------------------------------*\\\n")
    currentFile.write("| =========                 |                                                 |\n")
    currentFile.write("| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |\n")
    currentFile.write("|  \\\\    /   O peration     | Version:  2.3.0                                 |\n")
    currentFile.write("|   \\\\  /    A nd           | Web:      www.OpenFOAM.org                      |\n")
    currentFile.write("|    \\\\/     M anipulation  |                                                 |\n")
    currentFile.write("\*---------------------------------------------------------------------------*/\n")
    currentFile.write("FoamFile\n")
    currentFile.write("{\n")
    currentFile.write("    version     2.0;\n")
    currentFile.write("    format      ascii;\n")
    currentFile.write("    class       labelList;\n")
    currentFile.write("    note        \"nPoints: " + str(nPoints) + " nCells: " + str(nCells) + " nFaces: "+ str(nFaces) + " nInternalFaces: " + str(nFaces-nExternalFaces) + "\";\n")
    currentFile.write("    location    \"constant/polyMesh\";\n")
    currentFile.write("    object      owner;\n")
    currentFile.write("}\n")
    currentFile.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n")
    currentFile.write("\n")
    currentFile.write("\n")

    currentFile.write(str(nFaces)+"\n(\n")

    for i in range(nCells):
        owners = ownersOfInternalFacesOfCell(i, Nx, Ny, Nz)
        for owner in owners:
            currentFile.write(str(owner) + "\n")

    for i in range(nExternalFaces):
        currentFile.write(str(ownerOfExternalFace(i, Nx, Ny, Nz)) + "\n")

    currentFile.write(")\n")
    currentFile.write("\n")
    currentFile.write("\n")
    currentFile.write("// ************************************************************************* //\n")
    currentFile.close()

    return 0

def writePoints(nPoints, Nx, Ny, Nz, Lx, Ly, Lz):

    currentFileName = "constant/polyMesh/points"
    if not os.path.exists(os.path.dirname(currentFileName)):
        os.makedirs(os.path.dirname(currentFileName))
    currentFile = open(currentFileName, "w")

    currentFile.write("/*--------------------------------*- C++ -*----------------------------------*\\\n")
    currentFile.write("| =========                 |                                                 |\n")
    currentFile.write("| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |\n")
    currentFile.write("|  \\\\    /   O peration     | Version:  2.3.0                                 |\n")
    currentFile.write("|   \\\\  /    A nd           | Web:      www.OpenFOAM.org                      |\n")
    currentFile.write("|    \\\\/     M anipulation  |                                                 |\n")
    currentFile.write("\*---------------------------------------------------------------------------*/\n")
    currentFile.write("FoamFile\n")
    currentFile.write("{\n")
    currentFile.write("    version     2.0;\n")
    currentFile.write("    format      ascii;\n")
    currentFile.write("    class       vectorField;\n")
    currentFile.write("    location    \"constant/polyMesh\";\n")
    currentFile.write("    object      points;\n")
    currentFile.write("}\n")
    currentFile.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n")
    currentFile.write("\n")
    currentFile.write("\n")

    currentFile.write(str(nPoints)+"\n(\n")

    for i in range(nPoints):
        # Calculate point grid coordinates
        xi = i % (Nx+1)
        zi = int(i/((Nx+1)*(Ny+1)))
        yi = ((i-(Nx+1)*(Ny+1)*zi)-xi)/(Nx+1)
        # Write point spatial coordinates
        currentFile.write("(" + '{0:.10g}'.format(xi*Lx) + " " + '{0:.10g}'.format(yi*Ly) + " " + '{0:.10g}'.format(zi*Lz) + ")\n")

    currentFile.write(")\n")
    currentFile.write("\n")
    currentFile.write("\n")
    currentFile.write("// ************************************************************************* //\n")
    currentFile.close()

    return 0

def pointsOfInternalFacesOfCell(cellId, Nx, Ny, Nz):
    """Returns a vector of Points of cell internal faces with outward normals
    (1,0,0), (0,1,0), (0,0,1), in this order.
    """
    output = []

    # grid coordinates of the cell
    xCell = cellId % Nx
    zCell = int(cellId/(Nx*Ny))
    yCell = ((cellId-Nx*Ny*zCell)-xCell)/Nx

    if (xCell < Nx-1): # Check if face with outward normal (1,0,0) is internal
        point_1 = zCell*(Nx+1)*(Ny+1) + yCell*(Nx+1) + xCell + 1
        point_2 = point_1 + (Nx+1)
        point_3 = point_2 + (Nx+1)*(Ny+1)
        point_4 = point_1 + (Nx+1)*(Ny+1)
        output.append([point_1, point_2, point_3, point_4])

    if (yCell < Ny-1): # Check if face with outward normal (0,1,0) is internal
        point_1 = zCell*(Nx+1)*(Ny+1) + (yCell+1)*(Nx+1) + xCell
        point_2 = point_1 + (Nx+1)*(Ny+1)
        point_3 = point_2 + 1
        point_4 = point_1 + 1
        output.append([point_1, point_2, point_3, point_4])

    if (zCell < Nz-1): # Check if face with outward normal (0,0,1) is internal
        point_1 = (zCell+1)*(Nx+1)*(Ny+1) + yCell*(Nx+1) + xCell
        point_2 = point_1 + 1
        point_3 = point_2 + (Nx+1)
        point_4 = point_1 + (Nx+1)
        output.append([point_1, point_2, point_3, point_4])

    return output

def ownersOfInternalFacesOfCell(cellId, Nx, Ny, Nz):
    """Returns list of of owners of cell internal faces with outward normals
    (1,0,0), (0,1,0), (0,0,1), in this order.
    """
    output = []

    # grid coordinates of the cell
    xCell = cellId % Nx
    zCell = int(cellId/(Nx*Ny))
    yCell = ((cellId-Nx*Ny*zCell)-xCell)/Nx

    if (xCell < Nx-1): # Check if face with outward normal (1,0,0) is internal
        owner = cellId
        output.append(owner)

    if (yCell < Ny-1): # Check if face with outward normal (0,1,0) is internal
        owner = cellId
        output.append(owner)

    if (zCell < Nz-1): # Check if face with outward normal (0,0,1) is internal
        owner = cellId
        output.append(owner)

    return output

def neighboursOfInternalFacesOfCell(cellId, Nx, Ny, Nz):
    """Returns list of neighbours of cell internal faces with outward normals
    (1,0,0), (0,1,0), (0,0,1), in this order.
    """
    output = []

    # grid coordinates of the cell
    xCell = cellId % Nx
    zCell = int(cellId/(Nx*Ny))
    yCell = ((cellId-Nx*Ny*zCell)-xCell)/Nx

    if (xCell < Nx-1): # Check if face with outward normal (1,0,0) is internal
        neighbour = cellId + 1
        output.append(neighbour)

    if (yCell < Ny-1): # Check if face with outward normal (0,1,0) is internal
        neighbour = cellId + Nx
        output.append(neighbour)

    if (zCell < Nz-1): # Check if face with outward normal (0,0,1) is internal
        neighbour = cellId + Nx*Ny
        output.append(neighbour)

    return output

def pointsOfExternalFace(externalFaceId, Nx, Ny, Nz):
    """Returns points of face nInternalFaces+externalFaceId (which is an external face)
    """

    # The way to calculate the points depends on which face of the domain it belongs to:
    # The first Ny*Nz faces have outward normal (-1, 0, 0) (referred to as x-)
    # The next  Ny*Nz faces have outward normal ( 1, 0, 0) (referred to as x+)
    # The next  Nx*Nz faces have outward normal ( 0,-1, 0) (referred to as y-)
    # The next  Nx*Nz faces have outward normal ( 0, 1, 0) (referred to as y+)
    # The next  Nx*Ny faces have outward normal ( 0, 0,-1) (referred to as z-)
    # The next  Nx*Ny faces have outward normal ( 0, 0, 1) (referred to as z+)
    # The following tests check in which face of the domain the face is.
    if   (externalFaceId < Ny*Nz):
        normal = "x-"
    elif (externalFaceId < 2*Ny*Nz):
        normal = "x+"
    elif (externalFaceId < 2*Ny*Nz + Nx*Nz):
        normal = "y-"
    elif (externalFaceId < 2*Ny*Nz + 2*Nx*Nz):
        normal = "y+"
    elif (externalFaceId < 2*Ny*Nz + 2*Nx*Nz + Nx*Ny):
        normal = "z-"
    elif (externalFaceId < 2*Ny*Nz + 2*Nx*Nz + 2*Nx*Ny):
        normal = "z+"
    else:
        raise(ValueError("externalFaceId is too large"))

    # Now that the normal to the face is known, calculate the points in the
    # respective way. */
    if (normal == "x-"):
        temp  = externalFaceId                      #index on "x-" face
        zCell = int(temp/Ny)                        #z-grid-coordinate of face
        yCell = temp%Ny                             #y-grid-coordinate of face
        point_1 = zCell*(Nx+1)*(Ny+1) + yCell*(Nx+1)
        point_2 = point_1 + (Nx+1)*(Ny+1)
        point_3 = point_2 + (Nx+1)
        point_4 = point_1 + (Nx+1)
    elif (normal == "x+"):
        # To get the formula for point_1, start with the formula for point_1 of face x-,
        # and replace externalFaceId by (externalFaceId-Ny*Nz), and then add Nx
        temp  = externalFaceId - (Ny*Nz)
        zCell = int(temp/Ny)
        yCell = temp%Ny
        point_1 = zCell*(Nx+1)*(Ny+1) + yCell*(Nx+1) + Nx
        point_2 = point_1 + (Nx+1)
        point_3 = point_2 + (Nx+1)*(Ny+1)
        point_4 = point_1 + (Nx+1)*(Ny+1)
    elif (normal == "y-"):
        temp  = externalFaceId - (2*Ny*Nz)          #index on "y-" face
        xCell = int(temp/Nz)                        #x-grid-coordinate of face
        zCell = temp%Nz                             #z-grid-coordinate of face
        point_1 = zCell*(Nx+1)*(Ny+1) + xCell
        point_2 = point_1 + 1
        point_3 = point_2 + (Nx+1)*(Ny+1)
        point_4 = point_3 -1
    elif (normal == "y+"):
        # To get the formula for point_1, start with the formula for point_1 of face y-,
        # and replace externalFaceId by (externalFaceId-Nx*Nz), and then add (Nx+1)*Ny
        temp  = externalFaceId - (2*Ny*Nz + Nx*Nz)
        xCell = int(temp/Nz)
        zCell = temp%Nz
        point_1 = zCell*(Nx+1)*(Ny+1) + xCell + (Nx+1)*Ny
        point_2 = point_1 + (Nx+1)*(Ny+1)
        point_3 = point_2 + 1
        point_4 = point_1 + 1
    elif (normal == "z-"):
        temp  = externalFaceId - (2*Ny*Nz + 2*Nx*Nz)    #index on "z-" face
        xCell = int(temp/Ny)                            #x-grid-coordinate of face
        yCell = temp%Ny                                 #y-grid-coordinate of face
        point_1 = yCell*(Nx+1) + xCell
        point_2 = point_1 + (Nx+1)
        point_3 = point_2 + 1
        point_4 = point_1 + 1
    elif (normal == "z+"):
        # To get the formula for point_1, start with the formula for point_1 of face z-,
        # and replace externalFaceId by (externalFaceId-Nx*Ny), and then add (Nx+1)*(Ny+1)*Nz
        temp  = externalFaceId - (2*Ny*Nz + 2*Nx*Nz + Nx*Ny)
        xCell = int(temp/Ny)
        yCell = temp%Ny
        point_1 = yCell*(Nx+1) + xCell + (Nx+1)*(Ny+1)*Nz
        point_2 = point_1 + 1
        point_3 = point_2 + (Nx+1)
        point_4 = point_3 -1
    else:
        raise ValueError("Unexpected value for normal: " + normal)

    return [point_1, point_2, point_3, point_4]

def ownerOfExternalFace(externalFaceId, Nx, Ny, Nz):
    """ Returns owner of face nInternalFaces+externalFaceId (which is an external face)
    """

    # The way to calculate the owner depends on which face of the domain it belongs to:
    # The first Ny*Nz faces have outward normal (-1, 0, 0) (referred to as x-)
    # The next  Ny*Nz faces have outward normal ( 1, 0, 0) (referred to as x+)
    # The next  Nx*Nz faces have outward normal ( 0,-1, 0) (referred to as y-)
    # The next  Nx*Nz faces have outward normal ( 0, 1, 0) (referred to as y+)
    # The next  Nx*Ny faces have outward normal ( 0, 0,-1) (referred to as z-)
    # The next  Nx*Ny faces have outward normal ( 0, 0, 1) (referred to as z+)
    # The following tests check in which face of the domain the face is.
    if   (externalFaceId < Ny*Nz):
        normal = "x-"
    elif (externalFaceId < 2*Ny*Nz):
        normal = "x+"
    elif (externalFaceId < 2*Ny*Nz + Nx*Nz):
        normal = "y-"
    elif (externalFaceId < 2*Ny*Nz + 2*Nx*Nz):
        normal = "y+"
    elif (externalFaceId < 2*Ny*Nz + 2*Nx*Nz + Nx*Ny):
        normal = "z-"
    elif (externalFaceId < 2*Ny*Nz + 2*Nx*Nz + 2*Nx*Ny):
        normal = "z+"
    else:
        raise(ValueError("externalFaceId is too large"))

    # Now that the normal to the face is known, calculate the owner in the
    # respective way.
    if (normal == "x-"):
        temp  = externalFaceId                      #index on "x-" face
        zCell = int(temp/Ny)                        #z-grid-coordinate of face
        yCell = temp%Ny                             #y-grid-coordinate of face

        owner = yCell*Nx + zCell*(Nx*Ny)
    elif (normal == "x+"):
        temp  = externalFaceId - (Ny*Nz)
        zCell = int(temp/Ny)
        yCell = temp%Ny
        owner = yCell*Nx + zCell*(Nx*Ny) + (Nx-1)
    elif (normal == "y-"):
        temp  = externalFaceId - (2*Ny*Nz)          #index on "y-" face
        xCell = int(temp/Nz)                        #x-grid-coordinate of face
        zCell = temp%Nz                             #z-grid-coordinate of face
        owner = xCell + zCell*(Nx*Ny)
    elif (normal == "y+"):
        temp  = externalFaceId - (2*Ny*Nz + Nx*Nz)
        xCell = int(temp/Nz)
        zCell = temp%Nz
        owner = xCell + zCell*(Nx*Ny) + Nx*(Ny-1)
    elif (normal == "z-"):
        temp  = externalFaceId - (2*Ny*Nz + 2*Nx*Nz)    #index on "z-" face
        xCell = int(temp/Ny)                            #x-grid-coordinate of face
        yCell = temp%Ny                                 #y-grid-coordinate of face
        owner = xCell + yCell*Nx
    elif (normal == "z+"):
        temp  = externalFaceId - (2*Ny*Nz + 2*Nx*Nz + Nx*Ny)
        xCell = int(temp/Ny)
        yCell = temp%Ny
        owner = xCell + yCell*Nx + Nx*Ny*(Nz-1)
    else:
        raise ValueError("Unexpected value for normal: " + normal)

    return owner

def boxMesh(Nx, Ny, Nz, Lx, Ly, Lz):
    """Create files
        constant/polyMesh/boundary
        constant/polyMesh/faces
        constant/polyMesh/neighbour
        constant/polyMesh/owner
        constant/polyMesh/points
    that would be created by calling OpenFOAM's blockMesh using the following
    blockMeshDict dictionary:

        vertices
        (
            (0     0     0    )
            (Nx*Lx 0     0    )
            (Nx*Lx Ny*Ly 0    )
            (0     Ny*Ly 0    )
            (0     0     Nz*Lz)
            (Nx*Lx 0     Nz*Lz)
            (Nx*Lx Ny*Ly Nz*Lz)
            (0     Ny*Ly Nz*Lz)
        );

        blocks
        (
            hex (0 1 2 3 4 5 6 7) (Nx Ny Nz) simpleGrading (1 1 1)
        );

        edges
        (
        );

        boundary
        (
        );

        mergePatchPairs
        (
        );
    """

    # Calculate number of points, faces and cells
    nPoints        = (Nx+1)*(Ny+1)*(Nz+1)
    nCells         = Nx*Ny*Nz
    nFaces         = Nx*Ny*(Nz+1)+Nx*Nz*(Ny+1)+Ny*Nz*(Nx+1)
    nExternalFaces = 2*(Nx*Ny+Nx*Nz+Ny*Nz)

    # Write files
    writeBoundary(nFaces, nExternalFaces)
    writeFaces(nCells, nFaces, nExternalFaces, Nx, Ny, Nz)
    writeNeighbour(nPoints, nCells, nFaces, nExternalFaces, Nx, Ny, Nz)
    writeOwner(nPoints, nCells, nFaces, nExternalFaces, Nx, Ny, Nz)
    writePoints(nPoints, Nx, Ny, Nz, Lx, Ly, Lz)

if __name__ == "__main__":
    if (len(sys.argv)!=7): # argc should be 2 for correct execution
        # We print sys.argv[0] assuming it is the program name
        print "usage: " + sys.argv[0] + " Nx Ny Nz Lx Ly Lz"
    else:
        Nx = int  (sys.argv[1])
        Ny = int  (sys.argv[2])
        Nz = int  (sys.argv[3])
        Lx = float(sys.argv[4])
        Ly = float(sys.argv[5])
        Lz = float(sys.argv[6])
        boxMesh(Nx,Ny,Nz,Lx,Ly,Lz)
